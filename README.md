# Учебный проект по анализу данных для отдела АСУТП

Директории:

`/client` - frontend: веб-интерфейс Пользователя. Отображает данные в графическом виде: гистограммы, тепловые карты и др.

`/server` - backend: веб-сервер. Формирует массивы данных для анализа из XML-файлов.
