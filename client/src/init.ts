import * as echarts from 'echarts/core';
import 'echarts-gl';

import { Bar3DChart } from 'echarts-gl/charts';
import { Grid3DComponent } from 'echarts-gl/components';

import {
    TooltipComponent,
    TooltipComponentOption,
    GridComponent,
    GridComponentOption,
    VisualMapComponent,
    VisualMapComponentOption
} from 'echarts/components';
import { HeatmapChart, HeatmapSeriesOption } from 'echarts/charts';
import { CanvasRenderer } from 'echarts/renderers';

echarts.use([
    TooltipComponent,
    GridComponent,
    VisualMapComponent,
    HeatmapChart,
    Bar3DChart,
    Grid3DComponent,
    CanvasRenderer
]);

export type EChartsOption = echarts.ComposeOption<
| TooltipComponentOption
| GridComponentOption
| VisualMapComponentOption
| HeatmapSeriesOption
>;

export const ECharts = echarts;
