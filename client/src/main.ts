import "./init"
import { ECharts, EChartsOption } from "./init"

var heatMap = ECharts.init(document.getElementById("heatMap"));
var bar3D = ECharts.init(document.getElementById("bar3D"));

/** Генератор случайных чисел */
function randomInteger(min: number, max: number) {
    // получить случайное число от (min-0.5) до (max+0.5)
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand)
}

/** Ось X - время, с интервалом в 30 минут */
const halfHours: string[] = [];
for (let i = 0; i <= 47; i += 2) {
    halfHours.push(`${ Math.floor(i / 2) }:00`)
    halfHours.push(`${ Math.floor(i / 2) }:30`)
}
/** Ось Y - дни недели */
const days = ["Вс", "Сб", "Пт", "Чт", "Ср", "Вт", "Пн"];

/** Массив тестовых значений */
let data48: [number, number, number][] = [];
// for (let x = 0; x < 48; x++) {
//     for (let y = 0; y < 7; y++) {
//         data48.push([x, y, randomInteger(0, 10)])
//     }
// }


heatMap.setOption({
    tooltip: {
        position: "top"
    },
    grid: {
        height: "60%",
        top: "10%"
    },
    xAxis: {
        type: "category",
        data: halfHours,
        splitArea: {
            show: true
        }
    },
    yAxis: {
        type: "category",
        data: days,
        splitArea: {
            show: true
        }
    },
    visualMap: {
        min: 200,
        max: 400,
        calculable: true,
        orient: "horizontal",
        left: "center",
        bottom: "5%"
    },
    series: [
        {
            name: "pow48",
            type: "heatmap",
            data: data48,
            label: {
                show: true
            },
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowColor: "rgba(0, 0, 0, 0.5)"
                }
            }
        }
    ]
});


bar3D.setOption({
    tooltip: {},
    visualMap: {
        max: 400,
        min: 200,
        inRange: {
        color: [
            '#313695',
            '#4575b4',
            '#74add1',
            '#abd9e9',
            '#e0f3f8',
            '#ffffbf',
            '#fee090',
            '#fdae61',
            '#f46d43',
            '#d73027',
            '#a50026'
        ]
        }
    },
    xAxis3D: {
        type: 'category',
        data: halfHours
    },
    yAxis3D: {
        type: 'category',
        data: days
    },
    zAxis3D: {
        type: 'value'
    },
    grid3D: {
        boxWidth: 200,
        boxDepth: 80,
        viewControl: {
        // projection: 'orthographic'
        },
        light: {
            main: {
                intensity: 1.2,
                shadow: false
            },
            ambient: {
                intensity: 0.3
            }
        }
    },
    series: [
        {
            type: 'bar3D',
            data: data48,
            shading: 'lambert',
            label: {
                fontSize: 16,
                borderWidth: 1
            },
            emphasis: {
                label: {
                fontSize: 20,
                color: '#900'
                },
                itemStyle: {
                color: '#900'
                }
            }
        }
    ]
});

fetch("/data", {
    method: "GET"
}).then(response => {
    if (response.status >= 200 && response.status < 300) {
        return response.json()
    } else {
        throw new Error(response.statusText)
    }
}).then((data: any) => {
    //console.log(data)
    days.length = 0;

    let y = 0;
    data48.length = 0;
    for (let day in data) {
        console.log(day)
        days.push(day)

        for (let station in data[day]) {
            console.log("  ", station)

            //console.log(data[day])
            for (let counter in data[day][station]) {
                if (counter === "1-2СШ-10, ЯЧ 6 ТП-АЗС (A+) на 1-2СШ-10, ЯЧ 6 ТП-АЗС") {
                    console.log("    ", counter)
                    console.log("    ", data[day][station][counter])

                    let values = data[day][station][counter];

                    // *
                    for (let x = 0; x < 48; x++) {
                        data48.push([x, y, values[x]])
                    }
                }
            }
        }
        y++;
    }
    console.log(data48);

    heatMap.setOption({
        yAxis: {
            data: days,
        },
        series: [
            {
                data: data48,
            }
        ]
    })

    bar3D.setOption({
        yAxis3D: {
            data: days
        },
        series: [
            {
                data: data48,
            }
        ]
    })
});
