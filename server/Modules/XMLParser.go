package Modules

import (
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"

	"github.com/beevik/etree"
	"golang.org/x/text/encoding/charmap"
)

func Parser(XML string) (mapPoints map[string]map[string][]int, day string, err error) {
	doc := etree.NewDocument()
	doc.ReadSettings.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		switch charset {
		case "windows-1251":
			return charmap.Windows1251.NewDecoder().Reader(input), nil
		default:
			return nil, fmt.Errorf("unknown charset: %s", charset)
		}
	}

	//var err error
	if err = doc.ReadFromFile(XML); err != nil {
		log.Println(err)
		return nil, "", err
	}

	root := doc.SelectElement("message")

	mapPoints = make(map[string]map[string][]int)


	// Дата показаний
	if el_datetime := root.SelectElement("datetime"); el_datetime != nil {
		if el_day := el_datetime.SelectElement("day"); el_day == nil {
			err = errors.New("date is nil")
			log.Println(err)
			return nil, "", err
		} else {
			//fmt.Println("Дата:", el_day.Text())
			day = el_day.Text()
		}
	}

	//
	if el_area := root.SelectElement("area"); el_area != nil {

		// Подстанции
		for _, el_measuringpoint := range el_area.SelectElements("measuringpoint") {
			attr_name := el_measuringpoint.SelectAttrValue("name", "unknown")

			mapChannels := make(map[string][]int)

			// Счётчики
			for _, el_measuringchannel := range el_measuringpoint.SelectElements("measuringchannel") {
				attr_desc := el_measuringchannel.SelectAttrValue("desc", "unknown")

				values := make([]int, 48)

				// Значения
				for i, el_value := range el_measuringchannel.FindElements("./period/value") {
					values[i], err = strconv.Atoi(el_value.Text())
					if err != nil {
						log.Println(err)
						return nil, "", err
					}
				}

				mapChannels[attr_desc] = values
			}
			mapPoints[attr_name] = mapChannels
		}
	}

	return mapPoints, day, err
}
