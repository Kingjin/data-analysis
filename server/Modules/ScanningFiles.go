package Modules

import (
	"os"
	"path/filepath"
	"strings"
)

func Scan(directory string, date string) (files []string, err error) {
	files = make([]string, 0)
	err = filepath.Walk(directory, func(wPath string, info os.FileInfo, err error) error {
		xml := strings.Contains(wPath, ".xml")
		if xml == true {
			contain := strings.Contains(wPath, date)
			if contain == true {
				files = append(files, wPath)
			}
		}
		return nil
	})
	return files, err
}
