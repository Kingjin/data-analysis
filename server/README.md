# Веб-сервер
Формирует массивы данных для анализа из XML-файлов.

## Запуск
1. Установить Go https://go.dev
2. Открыть папку `server` через VSCode
3. Нажать <kbd>F5</kbd> - приложение запустится в режиме отладки
