package main

import (
	"Server/Routes"

	"github.com/gin-gonic/gin"
)

func main() {
	//date := ""
	// files, err := Modules.Scan("XML", date)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }

	// var json map[string]map[string][]int
	// for _, f := range files {

	// 	json, err = Modules.Parser(f)
	// 	if err != nil {
	// 		log.Println(err)
	// 		return
	// 	}
	// }
	// fmt.Println(json)

	r := gin.Default()
	r.Static("/public", "../client/public")

	r.LoadHTMLFiles("../client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/data", Routes.GetData)

	r.Run(":5000")
}
