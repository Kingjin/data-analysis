package Routes

import (
	"Server/Modules"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetData возвращает данные
func GetData(c *gin.Context) {
	date := ""
	files, err := Modules.Scan("XML", date)
	if err != nil {
		log.Println(err)
		return
	}

	jsons := make(map[string]map[string]map[string][]int, 0)
	var json map[string]map[string][]int
	for _, f := range files {
        var day string
		json, day, err = Modules.Parser(f)
		if err != nil {
			log.Println(err)
			return
		}
		//jsons = append(jsons, json)
		jsons[day] = json
	}

	c.JSON(http.StatusOK, jsons)
}
